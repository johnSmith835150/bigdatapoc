package com.jstanier.slacktokafka.message;
import org.json.simple.JSONObject;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.SlackUser;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SlackMessageProcessor {

    private static Logger logger = LoggerFactory.getLogger(SlackMessageProcessor.class);

    @Autowired
    private Producer<String, String> producer;

    @Value("${kafka.topic}")
    private String kafkaTopic;

    private ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    public void setup() {
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

    public void process(SlackMessagePosted message) {
        try {
	    String messageContent = message.getMessageContent();
            SlackUser messageSender = message.getSender();
	    //System.out.println(" sender " + messageSender.getUserName());
            SlackChannel channel = message.getChannel();
	    //System.out.println(" channel " + channel.getName());
	    JSONObject json = new JSONObject();
       	    json.put("message", messageContent);
	    json.put("sender" ,messageSender.getUserName());
	    json.put("channel",channel.getName());
            String jsonObject = objectMapper.writeValueAsString(json);
            producer.send(new KeyedMessage<>(kafkaTopic, jsonObject));
        } catch (JsonProcessingException e) {
            logger.warn("Error converting message to JSON", e);
        }
    }
}
