#!/usr/bin/python

# This sample executes a search request for the specified search term.
# Sample usage:
#   python search.py --q=surfing --max-results=10
# NOTE: To use the sample, you must provide a developer key obtained
#       in the Google APIs Console. Search for "REPLACE_ME" in this code
#       to find the correct place to provide that key..

import time
import argparse
from json import dumps
from flask import Flask
# from textblob import TextBlob
from kafka import KafkaProducer
from argparse import ArgumentParser
from flask import Flask, jsonify, request

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = 'AIzaSyBFvDy4kc2xO9-EhzMRvcJnA74HMrIJqDc'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092','192.168.23.141:9092','192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

app = Flask(__name__)

@app.route('/youtube', methods=['GET'])
def youtube_search():
    query = request.args.get('query')
    count = int(request.args.get('count'))
    topic = request.args.get('topic')

    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=count
    ).execute()

    videos = []

    # Add each result to the appropriate list, and then display the lists of
    # matching videos
    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            videos.append('%s (%s)' % (search_result['snippet']['title'], search_result['id']['videoId']))
    for video in videos:
        producer.send(topic, key=bytearray(str(time.time()), "UTF-8"), value=dumps(video))
    response = {'message': 'done'}
    return jsonify(response), 201


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5003, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port
    app.run(host='0.0.0.0', port=port)

