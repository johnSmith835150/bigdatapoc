import json
import base64
import numpy as np
from io import BytesIO
from kafka import KafkaConsumer
from argparse import ArgumentParser
from skimage.io import imsave, imread
import cv2

def createConsumer(topic):
        consumer = KafkaConsumer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.23.142:9092'],
                                        auto_offset_reset='earliest',
                                        value_deserializer=lambda m: json.loads(m.decode('utf-8')))

        consumer.subscribe([topic])
        i =0
        for message in consumer:
	        json_load = json.loads(message.value)
	        b = bytes(json_load["image"], 'utf-8')
	        image_decode = base64.decodebytes(b)
        	x = np.array(bytearray(image_decode)).reshape(json_load["shape"])
        	imsave(str(i)+'h.'+json_load["extention"],x)
	        print("image Saved")
	        i+=1
        	#print(json_load["extention"])
                


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-t', '--topic', default="facebook-images-hagar", type=str, help='kafka topic to listen too')
    
    args = parser.parse_args()
    topic = args.topic
    #query = args.query

    createConsumer(topic)
