import praw
import time
from json import dumps
from flask import Flask
from textblob import TextBlob
from kafka import KafkaProducer
from argparse import ArgumentParser
from flask import Flask, jsonify, request


producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

app = Flask(__name__)

@app.route('/reddit', methods=['GET'])
def query_reddit():
    query = request.args.get('query')
    count = int(request.args.get('count'))
    topic = request.args.get('topic')

    reddit = praw.Reddit(client_id='p3EgUQURjtxUgA',
                        client_secret='pS4P0epLO26Ba09dcC3OFD_AgtM',
                        user_agent='abdosoliman')

    posts = []

    all = reddit.subreddit("all")
    for i in all.search(query, limit=count):
        posts.append(i.title)

    for post in posts:
        producer.send(topic,key=bytearray(str(time.time()), "UTF-8"), value=dumps(post))

    response = {'message': 'done'}
    return jsonify(response), 201

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5001, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)

