import facebook
import urllib3
# from pylab import *
import  urllib 
from io import BytesIO
import numpy as np
from skimage.io import imsave, imread
import re
from kafka import KafkaProducer
import base64
from json import dumps , loads
import codecs
import time
from flask import Flask, jsonify, request
from argparse import ArgumentParser
import sys

producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092','192.168.23.141:9092','192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'),max_request_size = 2000000)

app = Flask(__name__)

token ='EAAJebaCwhKIBAA3JzHKjlglWKG3VX5phijd5YuboPk2UhgbzrkHTpm599ZCmmVCC3esgfoXzm46uTrsWdbZAXVuHwIHtuvLrOYXvDCeIeZBDZBEQ1mXdhPf1jmj0SZCSZAJUB5k9NEbL9rGEJtRKO9MgHaarsokaYNdyTymoa1Iv9oKXWaIvyO'
graph = facebook.GraphAPI(access_token=token, version = 3.1)

def downloadImage(url,ext='jpg'):
   data = BytesIO(urllib.request.urlopen(url).read())
   image = imread(data, format=ext)
   return image

def getSources():
    
    imagesJson = graph.request('/111552060188454?fields=albums{photos{source}}')
    imagesList = imagesJson['albums']['data']
    imageSource =[]
    for i in range(len(imagesList)):
        imageSource.append(imagesList[i]['photos']['data'][0]['source'])
    return imageSource

def getExtension(imageLink):
    regex= '\.(gif|jpg|jpeg|tiff|png)'
    ext = re.search(regex,imageLink).group(0)
    ext=ext[1:]
    return ext


@app.route('/facebookimages', methods=['GET'])
def getImages():
    topic = request.args.get('topic')
    imageSource = getSources()
    images=[]
    # images.append("")
    # images.append("")
    for i in range(len(imageSource)):
        
        exten= getExtension(imageSource[i])
        images.append(downloadImage(imageSource[i],exten))
        k= images[i].flatten()
        image_encode = base64.encodebytes(k)
 
        data ={"shape":images[i].shape ,"image":str(image_encode,'utf-8'),"extention":exten  }
        # if i ==0 or i==1:
        #     continue
        print(sys.getsizeof(dumps(data)))
        f=producer.send(topic,  value=dumps(data))
        # print(f)
        # result = f.get(timeout=60)
        # producer.flush()
        # print("DONE!!!!!")
        
    response = {'message': 'done'}
    return jsonify(response), 201

@app.route('/facebookcomments',methods=['GET'])
def getComments():
    topic = request.args.get('topic')
    # token ='EAAJebaCwhKIBAA3JzHKjlglWKG3VX5phijd5YuboPk2UhgbzrkHTpm599ZCmmVCC3esgfoXzm46uTrsWdbZAXVuHwIHtuvLrOYXvDCeIeZBDZBEQ1mXdhPf1jmj0SZCSZAJUB5k9NEbL9rGEJtRKO9MgHaarsokaYNdyTymoa1Iv9oKXWaIvyO'
    # graph = facebook.GraphAPI(access_token=token, version = 3.1)
    postsJson = graph.request('/111552060188454?fields=posts{comments}')

    commentsList = postsJson['posts']['data']

    for i in range(len(commentsList)):
        postComments = commentsList[i]['comments']['data']
        for j in range (len(postComments)):
            data ={"comment": postComments[j]['message'] , "timestamp": postComments[j]["created_time"]}
            producer.send(topic, key=bytearray(str(time.time()), "UTF-8"), value=dumps(data))
        
    response = {'message': 'done'}
    return jsonify(response), 201     

if __name__ == '__main__':  
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5005, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)

