from json import dumps
from flask import Flask
from kafka import KafkaProducer
from argparse import ArgumentParser
from flask import Flask, jsonify, request

import time
import tweepy
from tweepy import OAuthHandler

producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

app = Flask(__name__)

@app.route('/twitter', methods=['GET'])
def getTweets():
    query = request.args.get('query')
    count = int(request.args.get('count'))
    topic = request.args.get('topic')

    consumer_key = 'jBWQEfTGRpRgB1qlWjU6ESMdL'
    consumer_secret = '0KfwzJMdIMxYMkVlg3VAWkmEgN5HF2wmedFV0B5orYjNTSYXaT'
    access_token = '3024686985-Qpto5Km9g8FbyJxkP0FAHDMhlsKceiRdZyRgTOf'
    access_token_secret = 'XM53lRqgBaKqTRQItciOGx3GE8NXnmGNRtZ5oOdUFx841'
    api = 5
    # attempt authentication
    try:
        # create OAuthHandler object
        auth = OAuthHandler(consumer_key, consumer_secret)
        # set access token and secret
        auth.set_access_token(access_token, access_token_secret)
        # create tweepy API object to fetch tweets
        api = tweepy.API(auth)
    except:
        print("Error: Authentication Failed")

        '''
    Main function to fetch tweets and parse them.
    '''
    # empty list to store parsed tweets
    tweets = []

    try:
        # call twitter api to fetch tweets
        fetched_tweets = api.search(q = query, count = count)

        # parsing tweets one by one
        for tweet in fetched_tweets:
            # saving text of tweet
            if (tweet.lang == 'en' and tweet.text not in tweets):
                tweets.append(tweet.text)
    except:
        print("lol")

    non_empty_tweets = []
    for tweet in tweets:
        if len(tweet):
            non_empty_tweets.append(tweet)
    tweets = non_empty_tweets
   
    for tweet in tweets:
        data = {"text" : tweet , "query" : query}
        producer.send(topic, key=bytearray(str(time.time()), "UTF-8"), value=dumps(data))

    response = {'message': 'done'}
    return jsonify(response), 201

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5002, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)

