module.exports = async (db, Sequelize) =>
	db.define('rate',
		{
			userId: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			productId: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			rate: Sequelize.ENUM('0', '1', '2', '3', '4', '5')
		},
		{
			timestamps: false
		})
