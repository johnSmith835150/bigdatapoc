require('dotenv').config();
const Sequelize = require('sequelize');
const userModel = require('./user');
const productModel = require('./product');
const transactionModel = require('./transaction');
const invoiceModel = require('./invoice');

var exports = module.exports = {};

const db = new Sequelize({
    host: process.env.HOST,
    username: process.env.DB_ATTR,
    password: process.env.DB_PASSWORD,
    database: process.env.DATABASE,
    dialect: 'mysql'
});

userModel(db, Sequelize);
productModel(db, Sequelize);
transactionModel(db, Sequelize);
invoiceModel(db, Sequelize);

db.models.invoice.hasMany(db.models.transaction, {
    foreignKey: 'invoiceId'
});

db.models.transaction.belongsTo(db.models.invoice, {
    foreignKey: 'invoiceId'
});

db.models.transaction.belongsTo(db.models.product, {
    foreignKey: 'productId'
});

db.models.product.hasMany(db.models.transaction, {
    foreignKey: 'productId'
});

exports.sync = args => db.sync(args);
exports.sequelize = db;
exports.User = db.models.user;
exports.Product = db.models.product;
exports.Transaction = db.models.transaction;
exports.Invoice = db.models.invoice;
exports.Sequelize = Sequelize;
