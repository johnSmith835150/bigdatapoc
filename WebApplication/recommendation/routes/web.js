const express = require('express');
const router = express.Router();

var recommender = require('../controllers/recommendationController');

router.get('/recommend', recommender.recommend);

module.exports = router;
