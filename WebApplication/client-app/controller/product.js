const { PRODUCT_ENDPOINT, RECOMMENDATION_ENDPOINT } = require('../config/api_endpoint')

module.exports.home = async (req, res) => {
    try {
        const page = Number(req.query.page || 1);
        var userId = res.locals.userId;
        if (userId == null)
            userId = -1;

        const [home, popular] = await req.axios.all([
            req.axios.get(PRODUCT_ENDPOINT + "?page=" + page),
            req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`)
        ]);
        const {
            data: { data }
        } = home;
        
        res.render('home', {
            page: page,
            numPages: Math.ceil(Number(data.count) / 12),
            product: data.rows,
            popular: popular.data.data[0],
            totalCount: Number(data.count)
        });
    }
    catch (error) {
        console.error(error);
    }
}

module.exports.ownProduct = async (req, res) => {
    try {
        const {
            data: { data }
        } = await req.axios.get(`${PRODUCT_ENDPOINT}/own`);

        var userId = res.locals.userId;
        if (userId == null)
            userId = -1;
    
        let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
        data.popular = popular.data.data[0];
        
        res.render('products', data);
    }
    catch (err) {
        res.json({ err });
    }
}

module.exports.productDetail = async (req, res) => {
    try {
        const id = req.params.id;
        const {
            data: { data }
        } = await req.axios.get(PRODUCT_ENDPOINT + '/' + id);

        var userId = res.locals.userId;
        if (userId == null)
            userId = -1;
        
        let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
        data.popular = popular.data.data[0];
        
        if (data.userId === userId)
            return res.render('productOwnDetail', data);

        res.render('productDetail', data);
    }
    catch (err) {
        console.error(err);
    }
}

module.exports.createProduct = async (req, res) => {
    try {
        var userId = res.locals.userId;
        if (userId == null)
            userId = -1;

        let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);

        res.render('createProduct', { popular: popular.data.data[0] });
    }
    catch (err) {
        console.error(err);
    }
}

module.exports.createProductPost = async (req, res) => {
    try {
        var filename = "upload/default-product.jpg";
        if (req.file != undefined)
            filename = `upload/${req.file.filename}`;
        
        await req.axios.post(PRODUCT_ENDPOINT + '/create', {
            ...req.body,
            photoUrl: filename 
        
        });

        res.redirect('/products');
    }
    catch (err) {
        res.send(err);
        console.error(err);
    }
}

module.exports.deleteProduct = async (req, res) => {
    try {
        await req.axios.delete(`${PRODUCT_ENDPOINT}/${req.params.id}/delete`);
        res.redirect('/products');
    }
    catch (err) {
        res.send('There is a mistake');
        console.error(err);
    }
}

module.exports.editProduct = async (req, res) => {
    try {
        const data = { ...req.body };
        delete data.id;

        await req.axios.put(`${PRODUCT_ENDPOINT}/${req.params.id}/edit`, data);

        res.redirect('/product/' + req.params.id);
    }
    catch (err) {
        res.send('There is a mistake ' + err);
        console.error(err);
    }
}

module.exports.rateProduct = async (req, res) => {
    var response = null;
    try {
        var productId = req.body.id;

        response = await req.axios.post(PRODUCT_ENDPOINT + '/rate', {
            productId: productId,
            rate: req.body.rate
        });

        res.json({ isOk: true, newRate: response.data.newRate });
    }
    catch (err) {
        res.send(err);
        console.error(err);
    }
}

module.exports.searchProducts = async (req, res) => {
    try {
        const page = Number(req.query.page || 1);
    
        const query = req.query.query;
        const category = req.query.category;
        var userId = res.locals.userId;
            if (userId == null)
                userId = -1;

        const [home, popular] = await req.axios.all([
            req.axios.get(`${PRODUCT_ENDPOINT}/search?category=${category}&query=${query}&page=${page}`),
            req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`)
        ]);
        const {
            data: { data }
        } = home;

        res.render('search', {
            query: query,
            category: category,
            page: page,
            numPages: Math.ceil(Number(data.count) / 12),
            product: data.rows,
            popular: popular.data.data[0],
            totalCount: Number(data.count)
        });
    }
    catch (err) {
        res.send(err);
        console.error(err);
    }
}
