#!/bin/bash

mvn clean package
mvn exec:java -Dexec.mainClass="com.barq.abs.kafka.streams.sentimentAnalysis.SocialMediaStreamManipulation"
