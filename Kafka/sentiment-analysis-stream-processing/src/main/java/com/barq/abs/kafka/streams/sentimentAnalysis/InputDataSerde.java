package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class InputDataSerde implements Serde<InputData> {
    private InputDataSerializer serializer = new InputDataSerializer();
    private InputDataDeserializer deserializer = new InputDataDeserializer();


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<InputData> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<InputData> deserializer() {
        return deserializer;
    }
}
