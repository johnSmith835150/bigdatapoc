package com.barq.abs.kafka.streams.sentimentAnalysis;


import org.json.simple.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


public class SentimentAnalysis {

    public static String getSentiment(String input) {
        ConfigLoader.loadConfig();
        Config config = Config.getInstance();

        try {
            String url = config.sentimentAnalysisServiceUrl + encodeValue(input);

            Request request = new Request();
            JSONObject response = request.get(url);

            return (String) response.get("sentiment");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return "pos";
    }

    // Method to encode a string value using `UTF-8` encoding scheme
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
}
