package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.sql.Timestamp;

import static com.barq.abs.kafka.streams.sentimentAnalysis.SocialMediaStreamManipulation.serdeProps;

public class SocialMediaQueryResult {
    public String text;
    public String query;
    public Integer[] preprocessedText;
    public String language;
    public String timestamp;
    public String sentiment;

    public static final Serializer<SocialMediaQueryResult> SocialMediaQueryResultSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<SocialMediaQueryResult> SocialMediaQueryResultDeserializer = new JsonPOJODeserializer<>(SocialMediaQueryResult.class);


    public static Serde<SocialMediaQueryResult> SocialMediaQueryResultSerde;

    public SocialMediaQueryResult() {
//        do nothing
    }

    public SocialMediaQueryResult(String text, String query, Integer[] preprocessedText, String sentiment) {
        this.text = text;
        this.query = query;
        this.language = "en";
        this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
        this.sentiment = sentiment;
        this.preprocessedText = preprocessedText;
    }

    public static void createSerde() {
        if (SocialMediaQueryResultSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultDeserializer.configure(serdeProps, false);

        SocialMediaQueryResultSerde = Serdes.serdeFrom(SocialMediaQueryResultSerializer, SocialMediaQueryResultDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();

        json.put("text", text);
        json.put("query",query);
        json.put("language", language);
        json.put("timestamp", timestamp);
        json.put("sentiment", sentiment);
        json.put("preprocessedText", preprocessedText);

        return json.toString();
    }
}
