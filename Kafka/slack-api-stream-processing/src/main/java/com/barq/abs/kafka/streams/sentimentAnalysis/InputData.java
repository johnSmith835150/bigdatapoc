package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.io.Serializable;


public class InputData implements Serializable {
    public String sender;
    public String message;
    public String channel;

    public static final Serializer<InputData> InputDataSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<InputData> InputDataDeserializer = new JsonPOJODeserializer<>(InputData.class);


    public static Serde<InputData> InputDataSerde;

    public InputData() {
        // defualt constructor
    }

    public InputData(String sender, String message, String channel) {
        this.sender = sender;
        this.message = message;
        this.channel = channel;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("sender", sender);
        json.put("message", message);
        json.put("channel",channel);

        return json.toString();
    }

}
