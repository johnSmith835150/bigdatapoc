package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.sql.Timestamp;

import static com.barq.abs.kafka.streams.sentimentAnalysis.SocialMediaStreamManipulation.serdeProps;

public class SocialMediaQueryResult {
    public String text;
    public String user;
    public Integer[] preprocessedText;
    public String language;
    public String timestamp;
    public String sentiment;
    public Integer noOfWords;

    public static final Serializer<SocialMediaQueryResult> SocialMediaQueryResultSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<SocialMediaQueryResult> SocialMediaQueryResultDeserializer = new JsonPOJODeserializer<>(SocialMediaQueryResult.class);


    public static Serde<SocialMediaQueryResult> SocialMediaQueryResultSerde;

    public SocialMediaQueryResult() {
//        do nothing
    }

    public SocialMediaQueryResult(String text, String user, Integer[] preprocessedText, String sentiment,Integer noOfWords) {
        this.text = text;
        this.user = user;
        this.language = "en";
        this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
        this.sentiment = sentiment;
        this.preprocessedText = preprocessedText;
        this.noOfWords = noOfWords;
    }

    public static void createSerde() {
        if (SocialMediaQueryResultSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultDeserializer.configure(serdeProps, false);

        SocialMediaQueryResultSerde = Serdes.serdeFrom(SocialMediaQueryResultSerializer, SocialMediaQueryResultDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();

        json.put("text", text);
        json.put("user",user);
        json.put("language", language);
        json.put("timestamp", timestamp);
        json.put("sentiment", sentiment);
        json.put("preprocessedText", preprocessedText);
        json.put("noOfWords",noOfWords);

        return json.toString();
    }
}
