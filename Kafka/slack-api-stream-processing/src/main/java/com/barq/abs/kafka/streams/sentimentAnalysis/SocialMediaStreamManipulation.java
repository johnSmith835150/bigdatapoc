package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;


public class SocialMediaStreamManipulation {
    private static final String[] inputTopics = {"slack.messages"};// "youtube-topic"};
    private static final String outputTopic = "slack-output";
    public static Map<String, Object> serdeProps = new HashMap<>();


//    private static String [] inputTopics = null;
//    private static String outputTopic = null;
//    public static Map<String, Object> serdeProps = new HashMap<>();

    public static void main(final String[] args) {
//        ConfigLoader.loadConfig();
//        Config config = Config.getInstance();
//
//        inputTopics = config.inputTopics;
//        outputTopic = config.outputTopic;

        final Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "slack.messages");
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, "slack-output");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.23.140:9092 , 192.168.23.141:9092 , 192.168.23.142:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, InputDataSerde.class);
        properties.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        SocialMediaQueryResult.createSerde();

        final StreamsBuilder builder = new StreamsBuilder();
        process(builder); // all processing happens here
        final KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private static void process(final StreamsBuilder builder) {
        final KStream<String, InputData> source = builder.stream(Arrays.asList(inputTopics));

        KStream <String, SocialMediaQueryResult> classified = source.
                map((k, v) -> {
                    Random rand = new Random();
                    String sentiment = (rand.nextFloat() > 0.5) ? "pos" : "neg";

                    try {
                        sentiment = SentimentAnalysis.getSentiment(v.message);
                    }
                    catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    Integer [] features = ProcessNlp.process(v.message);
                    String text = ProcessNlp.removeSpecialCharacters(v.message);
                    Integer noOfWords = ProcessNlp.getSize(v.message);
                    SocialMediaQueryResult processed = new SocialMediaQueryResult(text, v.sender, features, sentiment,noOfWords);

                    return KeyValue.pair(k, processed);
                });

        classified.foreach( (k, v) -> System.out.println(v) );
        classified.to(outputTopic, Produced.with(Serdes.String(), SocialMediaQueryResult.SocialMediaQueryResultSerde));
    }
}
