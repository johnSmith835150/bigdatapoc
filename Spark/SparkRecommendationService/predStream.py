from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType

from pyspark.streaming.kafka import KafkaUtils
from kafka import SimpleProducer, KafkaClient
from kafka import KafkaProducer

from pyspark.sql import Row, HiveContext
from pyspark.sql import SQLContext, DataFrame
from pyspark.ml.recommendation import ALS, ALSModel
from pyspark.sql import Row
from operator import add

import json
import sys

loaded_model = None
producer = KafkaProducer(bootstrap_servers='192.168.23.140:9092')


def get_model(path):
    global loaded_model
    if loaded_model == None:
        loaded_model = ALSModel.load(path)


def getHiveContextInstance(sparkContext):
    if ('hiveContextSingletonInstance' not in globals()):
        globals()['hiveContextSingletonInstance'] = HiveContext(sparkContext)
    return globals()['hiveContextSingletonInstance']


def process(rdd):
    if rdd.isEmpty():
        return

    context = getHiveContextInstance(rdd.context)

    parsed = rdd.map(lambda w: json.loads(w[1]))
    data = parsed.map(lambda e: Row(
        text=str(e["preprocessedText"]), sentiment=str(e["sentiment"])))

    df = context.createDataFrame(data)
    #load_model = get_model('models/model_1')
    users = [{'Uid': 19}]
    users = context.createDataFrame(users)
    userSubsetRecs = loaded_model.recommendForUserSubset(users, 10)
    print("after predicition")
    recoreds = userSubsetRecs.select('recommendations').collect()
    for record in recoreds:
        print("inside the loop")
        print("inside: ", str(record))
        producer.send('spark.out', str(record))
        producer.flush()

    # df.write.mode('append').format("parquet").saveAsTable("yasmeen_table")


if __name__ == "__main__":

    topic = "result-processed"
    brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"

    sc = SparkContext(appName="streamPredicition")
    ssc = StreamingContext(sc, 1)
    sqlContext = SQLContext(sc)
    get_model('/user/root/models/model_1')
    kafkaParams = {"metadata.broker.list": brokers,
                   "auto.offset.reset": "smallest"}
    kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)

    kafkaDStream.foreachRDD(process)

    ssc.start()
    ssc.awaitTermination()
