from pyspark.sql import SparkSession
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS
from pyspark.sql import Row

spark = SparkSession.builder.master("yarn").appName('alsModel').getOrCreate()
df = spark.read.load('training2.csv', format ="csv" ,header='true', inferschema='true')

ratings = df 
training = df
#(training, test) = ratings.randomSplit([0., 0.2])
   
als = ALS(maxIter=25, regParam=0.1, userCol="userId", itemCol="productId", 
              alpha=1,implicitPrefs=False, ratingCol="rate",
          coldStartStrategy="drop")

model = als.fit(training)

model.write().overwrite().save("hdfs:///user/root/als2.model")
exit()
