from pyspark.sql import SparkSession
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS, ALSModel
from pyspark.sql import Row
from pyspark.sql.functions import udf
from pyspark.mllib.recommendation import MatrixFactorizationModel


NUM_RECOMMENDED_ITEMS = 3

spark = SparkSession.builder.master("yarn").appName('alsModel').getOrCreate()
model = ALSModel.load("hdfs:///user/root/als2.model")
userRecs = model.recommendForAllUsers(10)
id = 5
userRecs.show(5,truncate = False)
# user = spark.createDataFrame([(id,),(471,),(10,)], ["userId"])
# specificRecs = model.recommendForUserSubset(user, 20)

# productIds = specificRecs.select('recommendations').collect()
# specificRecs.show(5,truncate = False)

#print(productIds)
